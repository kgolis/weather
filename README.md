# Weather #

Simple application for checking weather from rest service (openweather).

This application is for learning purposes. 

### Some key features that are implemented: ###

* Connecting to API by Retrofit or Apache HttpClient 
* Getting and parsing JSON by Gson library
* Showing results 
* Some design patterns like strategy for selecting sync or async requests using RxJava